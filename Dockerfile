FROM python:3.8-slim-buster
# Aymane was here during the exam 02/03/2023
WORKDIR /app
# Wish me luck!
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --no-cache-dir
COPY . .

EXPOSE 80
CMD ["gunicorn", "--bind", "0.0.0.0:80", "app:app"]
